import { cars } from "../data/cars";
import { employees } from "../data/employees";

const search = <O>(object: O, properties: Array<keyof O>, query: string) => {
  return (
    query !== "" &&
    properties.some((property) => {
      const valueOfCompare = object[property];
      return (
        (typeof valueOfCompare === "string" ||
          typeof valueOfCompare === "number") &&
        valueOfCompare.toString().toLowerCase().includes(query.toLowerCase())
      );
    })
  );
};

const masseratis = cars.filter((car) => search(car, ["name"], "Maserati"));
console.log("Cars search:");
console.log(masseratis);

const onlyEmailsOrg = employees.filter((employee) =>
  search(employee, ["email"], ".org")
);
console.log("Employees search:");
console.log(onlyEmailsOrg);
