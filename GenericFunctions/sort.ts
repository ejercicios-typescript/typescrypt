import { cars } from "../data/cars";
import { employees } from "../data/employees";

const sort = <T>(prev: T, compare: T, property: keyof T) => {
  return prev[property] > compare[property]
    ? 1
    : prev[property] < compare[property]
    ? -1
    : 0;
};

const orderCars = cars.sort((prev, compare) => sort(prev, compare, "year"));
console.log("Cars sort:");
console.log(orderCars);

const orderEmployees = employees.sort((prev, compare) =>
  sort(prev, compare, "ip_address")
);
console.log("Employees sort:");
console.log(orderEmployees);
