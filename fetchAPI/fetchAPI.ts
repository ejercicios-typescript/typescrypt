import axios from "axios";

interface Geo {
    lat: string,
    lng: string
}

interface Address {
    street: string,
    suite: string,
    city: string,
    zipcode: string,
    geo: Geo
}

interface Company {
    name: string,
    catchPhrase: string,
    bs: string
}

const API_URL: string = "https://jsonplaceholder.typicode.com/users";

axios.get(`${API_URL}/1`).then(res => {
    const data = res.data as {
        id: number,
        name: string,
        username: string,
        email: string,
        address: Address,
        phone: string,
        website: string,
        company: Company
    };
    
    const {name,username,email,address,phone} = data;
    const {street,suite,zipcode,geo} = address;
    const {lat,lng} = geo;

    const message = printUser(name,username,email,street,suite,zipcode,lat,lng,formatPhone(phone));

    console.log(message);
});

const formatPhone = (phone: string): string[] | string => {
    if (phone.indexOf("x")) {
        return phone.split(" x");
    } else {
        return phone;
    }
};

const printUser = (name: string,username: string,email: string,street: string,suite: string,zipcode: string, lat: string, lng: string, phone: string | string[]): string => {
    let message: string = `
        La persona ${name} con el nombre de usuario ${username} y correo electrónico ${email}
        vive en: ${street} ${suite} ${zipcode} con las coordenadas ${lat}, ${lng}.
    `
    if (Array.isArray(phone)) {
        message += `    Puede contactarlo al teléfono: ${phone[0]} con extensión ${phone[1]}`
    } else {
        message += `    Puede contactarlo al teléfono: ${phone}`
    }
    
    return message
};