export interface Calculator {
  calculate(): number | number[];
  setFirstOperator(firstOperator: number): void;
  setSecondOperator(secondOperator: number): void;
}

class Totalizer implements Calculator {
  private firstOperator: number;
  private secondOperator: number;

  constructor(firstOperator: number, secondOperator: number) {
    this.firstOperator = firstOperator;
    this.secondOperator = secondOperator;
  }

  calculate(): number | number[] {
    return [this.firstOperator, this.secondOperator];
  }

  setFirstOperator(firstOperator: number): void {
    this.firstOperator = firstOperator;
  }

  setSecondOperator(secondOperator: number): void {
    this.secondOperator = secondOperator;
  }
}

export default Totalizer;
