import Totalizer from "./Totalizer";

class Multiply extends Totalizer {
  constructor(firstOperator: number, seconOperator: number) {
    super(firstOperator, seconOperator);
  }

  calculate(): number | number[] {
    const value = super.calculate();
    const [first, second] = typeof value === "object" ? value : [0, 0];
    return first * second;
  }
}

export default Multiply;
