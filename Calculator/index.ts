import Add from "./Add";
import Divide from "./Divide";
import Multiply from "./Multiply";
import Subtract from "./Subtract";
import Totalizer from "./Totalizer";

type operations = "+" | "-" | "*" | "/";

const getValuesOfTotalizer = (): {
  operators: string[];
  operatorSelected: operations;
} => {
  const operationsValid: Array<operations> = ["+", "-", "*", "/"];
  const expression = process.argv.filter((prss) => prss.includes("OP="))[0];
  const operation = expression.split("=")[1];

  let operators: string[] = [];
  let operatorSelected: operations = "+";
  operationsValid.forEach((operator) => {
    if (operation.includes(operator)) {
      operators = operation.split(operator);
      operatorSelected = operator;
    }
  });

  return { operators, operatorSelected };
};

const makeOperation = () => {
  const { operatorSelected, operators } = getValuesOfTotalizer();
  const [firstOperator, secondOperator] = operators;

  let returnedOperation: Totalizer;
  switch (operatorSelected) {
    case "+":
      returnedOperation = new Add(
        parseFloat(firstOperator),
        parseFloat(secondOperator)
      );
      break;
    case "-":
      returnedOperation = new Subtract(
        parseFloat(firstOperator),
        parseFloat(secondOperator)
      );
      break;
    case "*":
      returnedOperation = new Multiply(
        parseFloat(firstOperator),
        parseFloat(secondOperator)
      );
      break;
    case "/":
      returnedOperation = new Divide(
        parseFloat(firstOperator),
        parseFloat(secondOperator)
      );
      break;
  }
  return `El resultado de ${firstOperator} ${operatorSelected} ${secondOperator} es ${returnedOperation.calculate()}`;
};

console.log(makeOperation());
