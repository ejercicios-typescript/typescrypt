class Account {
    _headline: string;
    _amount?: number;

    constructor(headline: string,amount?: number) {
        this._headline = headline;
        this._amount = amount;
    }
    
    getHeadline() : string {
        return this._headline;
    }
    
    setHeadline(v : string) {
        this._headline = v;
    }

    getAmount() : number {
        if (typeof this._amount === "undefined") {
            return 0;
        }
        return this._amount;
    }
    
    setAmount(v : number) {
        this._amount = v;
    }

    deposit(amount: number) {
        // console.log(typeof this._amount)
        if ( typeof this._amount === "number" && amount > 0) {
            this._amount += amount;
        }
    }

    withDrawals(amount: number) {
        if ( typeof this._amount === "number" && amount < this._amount) {
            this._amount -= amount;
            return;
        }
        this._amount = 0;
    }
}

const a = new Account("Didier",0);
console.log(a.getHeadline());
console.log(a.getAmount());
a.setHeadline("Erick");
console.log(a.getHeadline());
console.log(a.getAmount());
a.deposit(32);
console.log(a.getAmount());
a.withDrawals(20);
console.log(a.getAmount());
a.deposit(-4);
console.log(a.getAmount());
a.withDrawals(90);
console.log(a.getAmount());