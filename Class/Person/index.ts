type Gender = "H" | "M";

class Person {
  private name: string = "";
  private age: number = 0;
  private curp: string;
  private gender: Gender = "H";
  private weight: number = 0;
  private height: number = 0;

  constructor(
    name: string,
    age: number,
    gender: Gender,
    weight: number,
    height: number
  ) {
    this.name = name;
    this.age = age;
    this.curp = this.createCurp();
    this.gender = this.checkGender(gender);
    this.weight = weight;
    this.height = height;
  }

  calculateBMI() {
    const BMI = this.weight / (this.height ^ 2);
    return BMI < 20 ? -1 : BMI >= 20 && BMI <= 25 ? 0 : 1;
  }

  isOlder() {
    return this.age >= 18 ? true : false;
  }

  checkGender(gender: Gender): Gender {
    const result = gender === "H" || gender === "M" ? gender : "H"
    // console.log(result);
    return result;
  }

  createCurp() {
    const vowels = ["a", "e", "i", "o", "u"];
    const fullName = this.name.split(" ", 3);
    // console.log(fullName);
    const [lastName1, lastName2, name] = fullName;
    let curp = lastName1[0];
    let stopFor = false;

    for (let i = 0; i < lastName1.length; i++) {
      for (let j = 0; j < vowels.length; j++) {
        if (vowels[j].toUpperCase() === lastName1[i].toUpperCase()) {
          curp += lastName1[i];
          stopFor = !stopFor;
          break;
        }
      }

      if (stopFor) {
        break;
      }
    }
    curp += lastName2[0] + name[0] + this.gender;
    // console.log(this.gender);
    return curp.toUpperCase();
  }

  toString() {
    const message = `Hola, soy ${this.name}, tengo ${this.age} años, mi género es ${this.gender}, peso ${this.weight} kg y mido ${this.height} teniendo ${this.calculateBMI() === -1 ? "peso bajo" : this.calculateBMI() === 0 ? "peso ideal" : "sobre peso"}`;
    console.log(message);
  }
}

const didier = new Person(
  "Hernández Victoriano Erick Didier",
  22,
  "H",
  60,
  1.7
);
didier.toString();
