import { Employee } from "../data/employees";
import { Car } from "../data/cars";

type keys = keyof Employee;

const uniqueKey: keys = "ip_address";

// pick
const employee: Pick<Employee, "first_name" | "email"> = {
  first_name: "",
  email: "",
};

const sendEmailGroup = (
  users: Pick<Employee, "email" | "first_name">
): Array<Pick<Employee, "email" | "first_name">> => {
  return [
    { first_name: "", email: "" },
    { first_name: "", email: "" },
    { first_name: "", email: "" },
    { first_name: "", email: "" },
    { first_name: "", email: "" },
    { first_name: "", email: "" },
    { first_name: "", email: "" },
    { first_name: "", email: "" },
  ];
};

// omit
const employee2: Omit<Employee, "first_name" | "id"> = {
  /* id: 1,
  first_name: "Chad", */
  last_name: "Ham",
  email: "cham0@google.com",
  gender: "Male",
  ip_address: "237.243.59.143",
};

// readonly
const printerUser = (
  employee: Readonly<Omit<Employee, "fisrt-name" | "id">>
) => {
  console.log(employee.email);
};

// record
type keysOfEmployee =
  | "id"
  | "first_name"
  | "last_name"
  | "email"
  | "gender"
  | "ip_address";

const createObjectPerson = (): Record<"email", Pick<Employee, "email">> => {
  return { email: { email: "" } };
};

// exclude
type whateverData = Employee | Car;

const excludeEx = (data: Exclude<whateverData, Employee>) => {
  data = {
    id: 1,
    model: "",
    name: "",
    VIN: "",
    year: 0,
  };
};
